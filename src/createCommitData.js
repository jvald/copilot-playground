/**
 * Create a commit data object from a commit.
 * @param {Object} commit The commit data.
 * @returns {Object} The commit data object.
 */
function createCommitData(commit) {
    return {
        id: commit.id,
        message: commit.message,
        author: commit.author_name,
        timestamp: commit.authored_date
    };
}

module.exports = createCommitData;