const GITLAB_API_URL = 'https://gitlab.com/api/v4/projects/3472737/repository/commits?per_page=1';

async function fetchLastCommit() {
    const response = await fetch(GITLAB_API_URL);
    if (!response.ok) {
        throw new Error(`HTTP error! status: ${response.status}`);
    }
    const data = await response.json();
    return data[0];
}

module.exports = fetchLastCommit;