const fs = require('fs');

/**
 * Save commit data to a file.
 * @param {Object} commitData The commit data to save.
 */
function saveCommitDataToFile(commitData) {
    fs.writeFileSync('last_commit.json', JSON.stringify(commitData, null, 2));
}

module.exports = saveCommitDataToFile;