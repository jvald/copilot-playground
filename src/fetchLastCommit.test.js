const fetchLastCommit = require('../src/fetchLastCommit');

describe('fetchLastCommit', () => {
  it('should fetch the last commit from the GitLab API', async () => {
    // Mock the fetch function
    global.fetch = jest.fn().mockResolvedValue({
      ok: true,
      status: 200,
      json: jest.fn().mockResolvedValue([{ commitId: 'abc123', message: 'Initial commit' }])
    });

    const lastCommit = await fetchLastCommit();
    const GITLAB_API_URL = 'https://gitlab.com/api/v4/projects/3472737/repository/commits?per_page=1';

    expect(fetch).toHaveBeenCalledWith(GITLAB_API_URL);
    expect(lastCommit).toEqual({ commitId: 'abc123', message: 'Initial commit' });
  });

  it('should throw an error if the response is not ok', async () => {
    // Mock the fetch function
    global.fetch = jest.fn().mockResolvedValue({
      ok: false,
      status: 404
    });

    await expect(fetchLastCommit()).rejects.toThrow('HTTP error! status: 404');
  });
});