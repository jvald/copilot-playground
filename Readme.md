# Project Title

This is a simple Node.js application that fetches the last commit from a GitLab repository and saves the commit data to a file.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites

- Node.js
- npm

### Installing

1. Clone the repository
```bash
git clone git@gitlab.com:jvald/copilot-playground.git
```

2. Navigate to the project directory
```bash
cd your-repo
```

3. Install dependencies
```bash
npm install
```

## Running the Application

To run the application, use the following command in the project directory:

```bash
node index.js
```

This will fetch the last commit from the specified GitLab repository and save the commit data to a file named [`last_commit.json`](command:_github.copilot.openRelativePath?%5B%22last_commit.json%22%5D "last_commit.json").

## Running the Tests

This project uses Jest for testing. To run the tests, use the following command:

```bash
npm test
```

## Built With

- [Node.js](https://nodejs.org/) - The runtime used
- [Jest](https://jestjs.io/) - The testing framework used

## Authors

- Your Name

## License

This project is licensed under the ISC License.

Please replace the following:
- `https://github.com/yourusername/your-repo.git` with the URL of your Git repository.
- `your-repo` with the name of your repository.
- `Your Name` with your name.