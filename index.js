const fs = require('fs');

const fetchLastCommit = require('./src/fetchLastCommit');
const createCommitData = require('./src/createCommitData');
const saveCommitDataToFile = require('./src/saveCommitDataToFile');

/**
 * Get the last commit and save the data to a file.
 */
async function getLastCommit() {
  try {
    const lastCommit = await fetchLastCommit();
    const commitData = createCommitData(lastCommit);
    saveCommitDataToFile(commitData);
    console.log('Last commit data saved to last_commit.json');
  } catch (error) {
    console.error('Error retrieving last commit:', error.message);
    // Write an empty object to the file to clear it.
    fs.writeFileSync('last_commit.json', JSON.stringify({})); 
  }
}

getLastCommit();